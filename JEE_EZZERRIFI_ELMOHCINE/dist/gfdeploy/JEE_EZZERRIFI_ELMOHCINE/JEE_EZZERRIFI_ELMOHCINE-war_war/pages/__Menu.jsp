<ul class="sidebar-menu">
                        <li>
                            <a href="index.html">
                                <i class="fa fa-home"></i> <span>Accueil</span>
                            </a>
                        </li>
                        
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Utilisateurs</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="GestionAnonymesServelet"><i class="fa fa-angle-double-right"></i>Anonymes</a></li>
                                <li><a href="GestionAssociationServelet"><i class="fa fa-angle-double-right"></i>Association de patients</a></li>
                                <li><a href="GestionMedecinServelet"><i class="fa fa-angle-double-right"></i>Médecins</a></li>
                                <li><a href="LaboratoireServelet"><i class="fa fa-angle-double-right"></i>Laboratoires</a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"></i>CRPV </a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"></i>CNPV </a></li>
                            </ul>
                        </li>


                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-plus-square"></i>
                                <span>Produits</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-angle-double-right"></i>Cosmétiques</a></li>
                                <li><a href="GestionMedicamentsServelet"><i class="fa fa-angle-double-right"></i>Médicaments</a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="GestionEffetsIndesirableServelet">
                                <i class="fa fa-bolt"></i> <span>Effets indésirables</span>
                            </a>
                        </li>
</ul>