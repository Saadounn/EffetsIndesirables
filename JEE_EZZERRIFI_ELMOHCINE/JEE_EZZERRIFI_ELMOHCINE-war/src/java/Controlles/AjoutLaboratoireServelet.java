/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlles;

import Entities.Laboratoires;
import Models.LaboratoiresFacadeLocal;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Saadoun
 */
@WebServlet(name = "AjoutLaboratoireServelet", urlPatterns = {"/AjoutLaboratoireServelet"})
public class AjoutLaboratoireServelet extends HttpServlet {

    @EJB
    public LaboratoiresFacadeLocal Labos;
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String s = request.getParameter("Action");
        
        if( s!=null && s.equals("newLab")){
            
            request.getRequestDispatcher("pages/Ajout_Laboratoire.jsp").forward(request, response);
            
        }else{
          if ( s!=null&& s.equals("valider")){
              
              Laboratoires newLab = new Laboratoires();
              
              newLab.setNom(request.getParameter("Nom"));
              newLab.setAdresse(request.getParameter("Adresse"));
              newLab.setType(request.getParameter("Type"));
              
              Labos.create(newLab);
              
                request.setAttribute("ListLaboratoires", Labos.findAll());
        
                request.getRequestDispatcher("pages/List_Laboratoires.jsp").forward(request, response);
              
            // Traitment d'insertion
        }else{
                
                request.getRequestDispatcher("pages/ErrorPage.jsp").forward(request, response); 
            }  
        }
        //*/
    }

    
    
    
    
    
    
    
    
    
    
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
