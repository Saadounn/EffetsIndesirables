/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlles;

import Entities.Medicament;
import Entities.Substanceactive;
import Models.MedicamentFacadeLocal;
import Models.SubstanceactiveFacade;
import Models.SubstanceactiveFacadeLocal;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.apache.taglibs.standard.functions.Functions.split;

/**
 *
 * @author Saadoun
 */
@WebServlet(name = "GestionMedicamentsServelet", urlPatterns = {"/GestionMedicamentsServelet"})
public class GestionMedicamentsServelet extends HttpServlet {

    
    
    @EJB
    public MedicamentFacadeLocal MedFcd;
    
    @EJB
    public SubstanceactiveFacadeLocal subFcd;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
      
  
        String action = request.getParameter("Action");        
        
        if( action!=null && action.equals(("Nouveau"))){
            request.getRequestDispatcher("pages/Ajout_Medicament.jsp").forward(request, response);
        }
        //*****************************
        if( action!=null && action.equals(("NouveauAction"))){
            
            Medicament medoc = new Medicament();
            
            medoc.setLibelle(request.getParameter("Libelle"));
            medoc.setIndication(request.getParameter("Indication"));
            // substances ==> voir la boucle
            medoc.setNomfabricant(request.getParameter("fabriquant"));
            medoc.setDureetraitement(request.getParameter("Duree"));
            medoc.setDateexpiration(request.getParameter("Date"));
            
            List<Substanceactive>subtances = new ArrayList<>();
            
            for( String subs : split(request.getParameter("Substances"),",")){
                Substanceactive sub = new Substanceactive();
                sub.setLibelle(subs);
                subtances.add(sub);
            }
            medoc.setSubstanceactiveList(subtances);
            List<Medicament> listMed = new ArrayList<>();
            listMed.add(medoc);
            
            for(Substanceactive subs : subtances){
                subs.setMedicamentList(listMed);
                subFcd.create(subs);
            }
            request.setAttribute("ListMedic", MedFcd.findAll());
            request.getRequestDispatcher("pages/List_Medicament.jsp").forward(request, response);

        }
        //*****************************
        if( action!=null && action.equals(("Edit"))){
            
            Medicament theMedoc = MedFcd.find(Integer.parseInt(request.getParameter("IdMedicament")));
            
            request.setAttribute("TheMedoc", theMedoc );
            request.getRequestDispatcher("pages/Modif_Medicament.jsp").forward(request, response);
            
            
        }
        
        
        //*****************************
        if( action!=null && action.equals(("Delete"))){
            
              if(null != MedFcd.find(Integer.parseInt(request.getParameter("IdMedicament")))){
            
                  Medicament theMedoc = MedFcd.find(Integer.parseInt(request.getParameter("IdMedicament")));
                  
                  MedFcd.remove(theMedoc);
                  
                  request.setAttribute("ListMedic", MedFcd.findAll());
                  request.getRequestDispatcher("pages/List_Medicament.jsp").forward(request, response);
              }
        }

            //*************Default*************
        request.setAttribute("ListMedic", MedFcd.findAll());
        request.getRequestDispatcher("pages/List_Medicament.jsp").forward(request, response);
        
        
    }


    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

  
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
