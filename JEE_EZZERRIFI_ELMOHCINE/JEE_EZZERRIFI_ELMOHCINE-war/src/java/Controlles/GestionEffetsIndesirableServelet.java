/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlles;

import Entities.Effetindesirable;
import Entities.Laboratoires;
import Entities.Medicament;
import Entities.Substanceactive;
import Entities.Useranonyme;
import Models.EffetindesirableFacadeLocal;
import Models.LaboratoiresFacadeLocal;
import Models.UseranonymeFacadeLocal;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.apache.taglibs.standard.functions.Functions.split;

/**
 *
 * @author Saadoun
 */
@WebServlet(name = "GestionEffetsIndesirableServelet", urlPatterns = {"/GestionEffetsIndesirableServelet"})
public class GestionEffetsIndesirableServelet extends HttpServlet {

     
    @EJB
    public EffetindesirableFacadeLocal Effets;
    public LaboratoiresFacadeLocal labFcd;
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
  
        
        String action = request.getParameter("Action");
        
        if( action!=null && action.equals(("AjouterMedoc"))){
            
            Effetindesirable effet = new Effetindesirable();
            effet.setResultatei(request.getParameter("Libelle"));
            effet.setDescriptionei(request.getParameter("Description"));
            effet.setDateeffet(request.getParameter("Date"));
            
            request.setAttribute("effet", effet);    
            request.getRequestDispatcher("pages/Ajout_Effet_Indesirable_Medoc.jsp").forward(request, response);
        }else if( action!=null && action.equals(("AjouterUsers"))){
            
           
            request.setAttribute("Libelle", request.getParameter("Libelle"));
            request.setAttribute("Description", request.getParameter("Description"));
            request.setAttribute("Date", request.getParameter("Date"));
            
            request.setAttribute("Libelle1", request.getParameter("Libelle1"));
            request.setAttribute("Indication1", request.getParameter("Indication1"));
            request.setAttribute("Substance1", request.getParameter("Substance1"));
            request.setAttribute("Fabricant1", request.getParameter("Fabricant1"));
            request.setAttribute("Duree1", request.getParameter("Duree1"));
            request.setAttribute("Date1", request.getParameter("Date1"));
            
            request.setAttribute("Libelle2", request.getParameter("Libelle2"));
            request.setAttribute("Indication2", request.getParameter("Indication2"));
            request.setAttribute("Substance2", request.getParameter("Substance2"));
            request.setAttribute("Fabricant2", request.getParameter("Fabricant2"));
            request.setAttribute("Duree2", request.getParameter("Duree2"));
            request.setAttribute("Date2", request.getParameter("Date2"));
            
            request.setAttribute("Libelle3", request.getParameter("Libelle3"));
            request.setAttribute("Indication3", request.getParameter("Indication3"));
            request.setAttribute("Substance3", request.getParameter("Substance3"));
            request.setAttribute("Fabricant3", request.getParameter("Fabricant3"));
            request.setAttribute("Duree3", request.getParameter("Duree3"));
            request.setAttribute("Date3", request.getParameter("Date3"));
            
            request.setAttribute("Libelle4", request.getParameter("Libelle4"));
            request.setAttribute("Indication4", request.getParameter("Indication4"));
            request.setAttribute("Substance4", request.getParameter("Substance4"));
            request.setAttribute("Fabricant4", request.getParameter("Fabricant4"));
            request.setAttribute("Duree4", request.getParameter("Duree4"));
            request.setAttribute("Date4", request.getParameter("Date4"));   
            
            request.setAttribute("Libelle5", request.getParameter("Libelle5"));
            request.setAttribute("Indication5", request.getParameter("Indication5"));
            request.setAttribute("Substance5", request.getParameter("Substance5"));
            request.setAttribute("Fabricant5", request.getParameter("Fabricant5"));
            request.setAttribute("Duree5", request.getParameter("Duree5"));
            request.setAttribute("Date5", request.getParameter("Date5"));            

            request.getRequestDispatcher("pages/Ajout_Effet_Indesirable_Users.jsp").forward(request, response);
        }else if( action!=null && action.equals(("Terminer"))){      
            
            Effetindesirable effet = new Effetindesirable();
            
            effet.setResultatei(request.getParameter("Libelle"));
            effet.setDescriptionei(request.getParameter("Description"));
            effet.setDateeffet(request.getParameter("Date"));
            
            
            
            
            Laboratoires lab = new Laboratoires();
            
            lab.setNom(request.getParameter("Nom"));
           // lab.setPrenom(request.getParameter("Prenom"));
            lab.setAdresse(request.getParameter("Adresse"));
            
            Medicament medoc1 = new Medicament();
//            Medicament medoc2 = new Medicament();
//            Medicament medoc3 = new Medicament();
//            Medicament medoc4 = new Medicament();
//            Medicament medoc5 = new Medicament();

            medoc1.setLibelle(request.getParameter("Libelle1"));
            medoc1.setIndication(request.getParameter("Indication1"));
            medoc1.setNomfabricant(request.getParameter("Fabricant1"));
            medoc1.setDureetraitement(request.getParameter("Duree1"));
            medoc1.setDateexpiration(request.getParameter("Date1"));    
                        
            List<Substanceactive> substances1 = new ArrayList<>();

            List<Medicament> l = new ArrayList();
            l.add(medoc1);
            for(String sub: split(request.getParameter("Substance1"),",")){
                
                Substanceactive S = new Substanceactive();
                S.setLibelle(sub);
                S.setMedicamentList(l);
                substances1.add(S);
            }
            
            medoc1.setSubstanceactiveList(substances1);    
            effet.setSubstanceactiveList(substances1);

            List<Effetindesirable> effList = new ArrayList<>();
            effList.add(effet);
            
            for(Substanceactive subs : substances1 ){
                subs.setEffetindesirableList(effList);
            }
            
            
            lab.setEffetindesirableList(effList);
            effet.setId(lab);
            
            
            labFcd.create(lab);
            
//            try (PrintWriter out = response.getWriter()) {
//
//                out.println("Libelle : "+  request.getParameter("Libelle")+"<br>");
//                out.println("Description : "+  request.getParameter("Description")+"<br>");
//                out.println("Date : "+  request.getParameter("Date")+"<br>");
//                
//                out.println("Medoc1  : "+ medoc1.toString()+"<br>");
//                out.println("Medoc2  : "+ medoc2.toString()+"<br>");
//                out.println("Medoc3  : "+ medoc3.toString()+"<br>");
//                out.println("Medoc4  : "+ medoc4.toString()+"<br>");
//                out.println("Medoc5  : "+ medoc5.toString()+"<br>");
//                
//
//                out.println("Nom : "+  request.getParameter("Nom")+"<br>");        
//                out.println("Prenom : "+  request.getParameter("Prenom")+"<br>");        
//                out.println("Adresse : "+  request.getParameter("Adresse")+"<br>");        
//                
//                
//                
//                
//            }
            
        request.setAttribute("ListEffets", Effets.findAll());
        request.getRequestDispatcher("pages/List_Effets_Indesirables.jsp").forward(request, response);

        if( action!=null && action.equals(("AjouterEffet"))){
            
            request.getRequestDispatcher("pages/Ajout_Effet_Indesirable.jsp").forward(request, response);
        }
        
        }
        
        
        
            //*************Default*************
        request.setAttribute("ListEffets", Effets.findAll());
        request.getRequestDispatcher("pages/List_Effets_Indesirables.jsp").forward(request, response);
        
        
        
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
/*
            try (PrintWriter out = response.getWriter()) {
                out.println("Libelle1 : " + request.getParameter("Libelle1")+"<br>");
            }
            //*/