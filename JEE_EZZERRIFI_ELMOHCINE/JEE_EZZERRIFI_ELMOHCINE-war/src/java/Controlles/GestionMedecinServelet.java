/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlles;

import Entities.Medecin;
import Models.MedecinFacadeLocal;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Saadoun
 */
@WebServlet(name = "GestionMedecinServelet", urlPatterns = {"/GestionMedecinServelet"})
public class GestionMedecinServelet extends HttpServlet {

 
    @EJB
    public MedecinFacadeLocal Medecins;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       
         
        String action = request.getParameter("Action");
        
        if( action!=null && action.equals(("Nouveau"))){
    
            request.getRequestDispatcher("pages/Ajout_Medecin.jsp").forward(request, response);
        }
        //********************
        if( action!=null && action.equals(("NouveauAction"))){
    
            
            Medecin medecin = new Medecin();
            
            medecin.setNom(request.getParameter("Nom"));
            medecin.setPrenom(request.getParameter("Prenom"));
            medecin.setAdresse(request.getParameter("Adresse"));
            medecin.setSpecialite(request.getParameter("Specialite"));
            
            Medecins.create(medecin);
            request.setAttribute("ListMedecin", Medecins.findAll());
 
            request.getRequestDispatcher("pages/List_Medecin.jsp").forward(request, response);
        }        
        //********************
        if( action!=null && action.equals(("Delete"))){
            
            if(null != Medecins.find(Integer.parseInt(request.getParameter("IdMedecin")))){
            Medecin theMedecin = Medecins.find(Integer.parseInt(request.getParameter("IdMedecin")));
            
            Medecins.remove(theMedecin);
            
            
            request.setAttribute("ListMedecin", Medecins.findAll());
            request.getRequestDispatcher("pages/List_Medecin.jsp").forward(request, response);
            
            }
        request.getRequestDispatcher("pages/Error.jsp").forward(request, response);
        }
        //*************Default*************
        request.setAttribute("ListMedecin", Medecins.findAll());
        request.getRequestDispatcher("pages/List_Medecin.jsp").forward(request, response);
        
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
