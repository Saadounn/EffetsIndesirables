/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlles;

import Entities.Useranonyme;
import Models.UseranonymeFacadeLocal;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Saadoun
 */
@WebServlet(name = "GestionAnonymesServelet", urlPatterns = {"/GestionAnonymesServelet"})
public class GestionAnonymesServelet extends HttpServlet {

    
    @EJB
    public UseranonymeFacadeLocal Anonyms; 
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        
        String action = request.getParameter("Action");
        
        if( action!=null && action.equals(("Nouveau"))){

            request.getRequestDispatcher("pages/Ajout_Anonyme.jsp").forward(request, response);
        }
        //*****************************
        if( action!=null && action.equals(("NouveauAction"))){

            Useranonyme anonyme = new Useranonyme();
            
            anonyme.setNom(request.getParameter("Nom"));
            anonyme.setPrenom(request.getParameter("Prenom"));
            anonyme.setAdresse(request.getParameter("Adresse"));
            
            Anonyms.create(anonyme);
            
            request.setAttribute("ListAnonymes", Anonyms.findAll());
            request.getRequestDispatcher("pages/List_Anonymes.jsp").forward(request, response);
        }
        //*****************************
        if( action!=null && action.equals(("Delete"))){
            
            if( null != Anonyms.find(Integer.parseInt(request.getParameter("IdAnonyme")))){
                Useranonyme theAno = Anonyms.find(Integer.parseInt(request.getParameter("IdAnonyme")));
                
                Anonyms.remove(theAno);
                
                
                request.setAttribute("ListAnonymes", Anonyms.findAll());
                request.getRequestDispatcher("pages/List_Anonymes.jsp").forward(request, response);
            }
                request.getRequestDispatcher("pages/Error.jsp").forward(request, response);
        }
        //*************Default*************
            request.setAttribute("ListAnonymes", Anonyms.findAll());
            request.getRequestDispatcher("pages/List_Anonymes.jsp").forward(request, response);
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
