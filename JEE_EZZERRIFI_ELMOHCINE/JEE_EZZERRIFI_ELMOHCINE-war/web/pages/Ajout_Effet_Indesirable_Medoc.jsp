<%@page import="Entities.Effetindesirable"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>NotifEI | Ajout des medicament pharmaceutiques</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <!-- DATA TABLES -->
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="index.html" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                NotifEI
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">


             
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>Jane Doe <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="img/avatar2.png" class="img-circle" alt="User Image" />
                                    <p>
                                        Jane Doe - Web Developer
                                        <small>Member since Nov. 2012</small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src=" img/avatar2.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, Jane</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="post" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Rechercher..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    
                    <%@include file="__Menu.jsp" %>


                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Medicament 
                        <small>Ajout un médicament</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Utilisateurs</a></li>
                        <li><a href="#">Medicament</a></li>
                        <li class="active">Nouveau</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <script> var compt = 2; </script>
                            <form action="GestionEffetsIndesirableServelet" method="post">
                                <% Effetindesirable effet = (Effetindesirable) request.getAttribute("effet");  %>
                                <input type="hidden" name="Libelle" value="<%=effet.getResultatei() %>" >
                                <input type="hidden" name="Description" value="<%=effet.getDescriptionei()%>" >
                                <input type="hidden" name="Date" value="<%=effet.getDateeffet()%>" >
                                
                                
                                
                                <div class="box box-primary" id="Box1">
                                <div class="box-header">
                                    <h3 class="box-title" id="Titre1" >Ajouter médicament</h3>
                                    <div class="pull-right box-tools">
                                        <a class="btn btn-primary btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></a>
                                    </div>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="Libelle1">Libellé *: </label>
                                            <input type="text" 
                                                   name ="Libelle1" 
                                                   class="form-control" 
                                                   id="Libelle1" 
                                                   placeholder="Nom du médicament ..."
                                                   onchange='{
                                                       var libelle = document.getElementById("Libelle1").value;
                                                       if(libelle != null) document.getElementById("Titre1").innerHTML = libelle;
                                                       else document.getElementById("Titre1").innerHTML = "Ajouter médicament";
                                                   }'
                                                   
                                                   >
                                        </div>
                                        <div class="form-group">
                                            <label for="Indication1">Indication : </label>
                                            <input type="text" name ="Indication1" class="form-control" id="Indication1" placeholder="Indications de la notice ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="Substance1">Substance(s) active(s) : </label>
                                            <input type="text" name ="Substance1" class="form-control" id="Substance1" placeholder="Substance active 1, Substance active 2, ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="Fabricant1">Nom du fablricant : </label>
                                            <input type="text" name ="Fabricant1" class="form-control" id="Fabricant1" placeholder="Nom du laboratoire fabricant ...">
                                        </div>
                                        
                                        <div class="form-group col-md-3">
                                            <label for="Duree1">Durée : </label>
                                            <input type="number" name ="Duree1" class="form-control" id="Duree1" placeholder="(En jours) ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="Date1">Date d'expiration : </label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="date" name="Date1" id="Date1" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" placeholder="jj/mm/aaaa">
                                            </div><!-- /.input group -->
                                        </div>
                                        
                                        
                                    </div><!-- /.box-body -->

                            </div><!-- /.box1 -->
                             
                                <div class="box box-primary" id="Box2" style="display:none">
                                <div class="box-header">
                                    <h3 class="box-title" id="Titre2" >Ajouter médicament</h3>
                                    <div class="pull-right box-tools">
                                        <a class="btn btn-primary btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></a>
                                        <a class="btn btn-primary btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove" onclick='document.getElementById("Box2").innerHTML = "";document.getElementById("Box2").style.display = "none"'><i class="fa fa-times" ></i></a>
                                    </div>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="Libelle2">Libellé *: </label>
                                            <input type="text" 
                                                   name ="Libelle2" 
                                                   class="form-control" 
                                                   id="Libelle2" 
                                                   placeholder="Nom du médicament ..."
                                                   onchange='{
                                                       var libelle = document.getElementById("Libelle2").value;
                                                       if(libelle != null) document.getElementById("Titre2").innerHTML = libelle;
                                                       else document.getElementById("Titre2").innerHTML = "Ajouter médicament";
                                                   }'
                                                   
                                                   >
                                        </div>
                                        <div class="form-group">
                                            <label for="Indication2">Indication : </label>
                                            <input type="text" name ="Indication2" class="form-control" id="Indication2" placeholder="Indications de la notice ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="Substance2">Substance(s) active(s) : </label>
                                            <input type="text" name ="Substance2" class="form-control" id="Substance2" placeholder="Substance active 1, Substance active 2, ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="Fabricant2">Nom du fablricant : </label>
                                            <input type="text" name ="Fabricant2" class="form-control" id="Fabricant2" placeholder="Nom du laboratoire fabricant ...">
                                        </div>
                                        
                                        <div class="form-group col-md-3">
                                            <label for="Duree2">Durée : </label>
                                            <input type="number" name ="Duree2" class="form-control" id="Duree2" placeholder="(En jours) ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="Date2">Date d'expiration : </label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="date" name="Date2" id="Date2" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" placeholder="jj/mm/aaaa">
                                            </div><!-- /.input group -->
                                        </div>
                                        
                                        
                                    </div><!-- /.box-body -->

                            </div><!-- /.box2 -->
                            
                                <div class="box box-primary" id="Box3" style="display:none">
                                <div class="box-header">
                                    <h3 class="box-title" id="Titre3" >Ajouter médicament</h3>
                                    <div class="pull-right box-tools">
                                        <a class="btn btn-primary btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></a>
                                        <a class="btn btn-primary btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove" onclick='document.getElementById("Box3").innerHTML = "";document.getElementById("Box3").style.display = "none"'><i class="fa fa-times" ></i></a>
                                    </div>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="Libelle3">Libellé *: </label>
                                            <input type="text" 
                                                   name ="Libelle3" 
                                                   class="form-control" 
                                                   id="Libelle3" 
                                                   placeholder="Nom du médicament ..."
                                                   onchange='{
                                                       var libelle = document.getElementById("Libelle3").value;
                                                       if(libelle != null) document.getElementById("Titre3").innerHTML = libelle;
                                                       else document.getElementById("Titre3").innerHTML = "Ajouter médicament";
                                                   }'
                                                   
                                                   >
                                        </div>
                                        <div class="form-group">
                                            <label for="Indication3">Indication : </label>
                                            <input type="text" name ="Indication3" class="form-control" id="Indication3" placeholder="Indications de la notice ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="Substance3">Substance(s) active(s) : </label>
                                            <input type="text" name ="Substance3" class="form-control" id="Substance3" placeholder="Substance active 1, Substance active 2, ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="Fabricant3">Nom du fablricant : </label>
                                            <input type="text" name ="Fabricant3" class="form-control" id="Fabricant3" placeholder="Nom du laboratoire fabricant ...">
                                        </div>
                                        
                                        <div class="form-group col-md-3">
                                            <label for="Duree3">Durée : </label>
                                            <input type="number" name ="Duree3" class="form-control" id="Duree3" placeholder="(En jours) ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="Date3">Date d'expiration : </label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="date" name="Date3" id="Date3" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" placeholder="jj/mm/aaaa">
                                            </div><!-- /.input group -->
                                        </div>
                                        
                                        
                                    </div><!-- /.box-body -->

                            </div><!-- /.box3 -->
                            
                                <div class="box box-primary" id="Box4" style="display:none">
                                <div class="box-header">
                                    <h3 class="box-title" id="Titre4" >Ajouter médicament</h3>
                                    <div class="pull-right box-tools">
                                        <a class="btn btn-primary btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></a>
                                        <a class="btn btn-primary btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove" onclick='document.getElementById("Box4").innerHTML = "";document.getElementById("Box4").style.display = "none"'><i class="fa fa-times" ></i></a>
                                    </div>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="Libelle4">Libellé *: </label>
                                            <input type="text" 
                                                   name ="Libelle4" 
                                                   class="form-control" 
                                                   id="Libelle4" 
                                                   placeholder="Nom du médicament ..."
                                                   onchange='{
                                                       var libelle = document.getElementById("Libelle4").value;
                                                       if(libelle != null) document.getElementById("Titre4").innerHTML = libelle;
                                                       else document.getElementById("Titre4").innerHTML = "Ajouter médicament";
                                                   }'
                                                   
                                                   >
                                        </div>
                                        <div class="form-group">
                                            <label for="Indication4">Indication : </label>
                                            <input type="text" name ="Indication4" class="form-control" id="Indication4" placeholder="Indications de la notice ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="Substance4">Substance(s) active(s) : </label>
                                            <input type="text" name ="Substance4" class="form-control" id="Substance4" placeholder="Substance active 1, Substance active 2, ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="Fabricant4">Nom du fablricant : </label>
                                            <input type="text" name ="Fabricant4" class="form-control" id="Fabricant4" placeholder="Nom du laboratoire fabricant ...">
                                        </div>
                                        
                                        <div class="form-group col-md-3">
                                            <label for="Duree4">Durée : </label>
                                            <input type="number" name ="Duree4" class="form-control" id="Duree4" placeholder="(En jours) ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="Date4">Date d'expiration : </label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="date" name="Date4" id="Date4" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" placeholder="jj/mm/aaaa">
                                            </div><!-- /.input group -->
                                        </div>
                                        
                                        
                                    </div><!-- /.box-body -->

                            </div><!-- /.box4 -->
                            
                                <div class="box box-primary" id="Box5" style="display:none">
                                <div class="box-header">
                                    <h3 class="box-title" id="Titre5" >Ajouter médicament</h3>
                                    <div class="pull-right box-tools">
                                        <a class="btn btn-primary btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></a>
                                        <a class="btn btn-primary btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove" onclick='document.getElementById("Box5").innerHTML = "";document.getElementById("Box5").style.display = "none"'><i class="fa fa-times" ></i></a>
                                    
                                    </div>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="Libelle5">Libellé *: </label>
                                            <input type="text" 
                                                   name ="Libelle5" 
                                                   class="form-control" 
                                                   id="Libelle5" 
                                                   placeholder="Nom du médicament ..."
                                                   onchange='{
                                                       var libelle = document.getElementById("Libelle5").value;
                                                       if(libelle != null) document.getElementById("Titre5").innerHTML = libelle;
                                                       else document.getElementById("Titre5").innerHTML = "Ajouter médicament";
                                                   }'
                                                   
                                                   >
                                        </div>
                                        <div class="form-group">
                                            <label for="Indication5">Indication : </label>
                                            <input type="text" name ="Indication5" class="form-control" id="Indication5" placeholder="Indications de la notice ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="Substance5">Substance(s) active(s) : </label>
                                            <input type="text" name ="Substance5" class="form-control" id="Substance5" placeholder="Substance active 1, Substance active 2, ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="Fabricant5">Nom du fablricant : </label>
                                            <input type="text" name ="Fabricant5" class="form-control" id="Fabricant5" placeholder="Nom du laboratoire fabricant ...">
                                        </div>
                                        
                                        <div class="form-group col-md-3">
                                            <label for="Duree5">Durée : </label>
                                            <input type="number" name ="Duree5" class="form-control" id="Duree5" placeholder="(En jours) ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="Date5">Date d'expiration : </label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="date" name="Date5" id="Date5" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" placeholder="jj/mm/aaaa">
                                            </div><!-- /.input group -->
                                        </div>
                                        
                                        
                                    </div><!-- /.box-body -->

                            </div><!-- /.box5 -->
                            
                            <div id="Box6" style="display:none">
                                <center>
                                    <h2 class="danger">Désolé ....</h2>
                                    <h3>Impossible d'ajouter d'autres médicaments</h3>
                                </center>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right" name="Action" value="AjouterUsers">Etape suivante</button>
                            </form>
                            
                            
                            <button class="btn btn-success pull-left"
                                    onclick='{
                                        document.getElementById("Box"+compt).style.display = "block";  
                                        compt++;
                                    }'
                                    ><i class="fa fa-plus"></i> Ajouter médicament</button>
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
            
            
        </script>

    </body>
</html>