/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Entities.Crpv;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Saadoun
 */
@Local
public interface CrpvFacadeLocal {

    void create(Crpv crpv);

    void edit(Crpv crpv);

    void remove(Crpv crpv);

    Crpv find(Object id);

    List<Crpv> findAll();

    List<Crpv> findRange(int[] range);

    int count();
    
}
