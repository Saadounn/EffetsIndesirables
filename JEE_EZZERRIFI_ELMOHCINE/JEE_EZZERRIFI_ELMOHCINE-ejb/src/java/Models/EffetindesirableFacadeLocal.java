/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Entities.Effetindesirable;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Saadoun
 */
@Local
public interface EffetindesirableFacadeLocal {

    void create(Effetindesirable effetindesirable);

    void edit(Effetindesirable effetindesirable);

    void remove(Effetindesirable effetindesirable);

    Effetindesirable find(Object id);

    List<Effetindesirable> findAll();

    List<Effetindesirable> findRange(int[] range);

    int count();
    
}
