/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Entities.Prodcosmetique;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Saadoun
 */
@Local
public interface ProdcosmetiqueFacadeLocal {

    void create(Prodcosmetique prodcosmetique);

    void edit(Prodcosmetique prodcosmetique);

    void remove(Prodcosmetique prodcosmetique);

    Prodcosmetique find(Object id);

    List<Prodcosmetique> findAll();

    List<Prodcosmetique> findRange(int[] range);

    int count();
    
}
