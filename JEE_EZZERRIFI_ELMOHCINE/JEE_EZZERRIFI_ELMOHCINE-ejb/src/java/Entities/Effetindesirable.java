/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Saadoun
 */
@Entity
@Table(name = "EFFETINDESIRABLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Effetindesirable.findAll", query = "SELECT e FROM Effetindesirable e"),
    @NamedQuery(name = "Effetindesirable.findByIdei", query = "SELECT e FROM Effetindesirable e WHERE e.idei = :idei"),
    @NamedQuery(name = "Effetindesirable.findByResultatei", query = "SELECT e FROM Effetindesirable e WHERE e.resultatei = :resultatei"),
    @NamedQuery(name = "Effetindesirable.findByDescriptionei", query = "SELECT e FROM Effetindesirable e WHERE e.descriptionei = :descriptionei"),
    @NamedQuery(name = "Effetindesirable.findByDateeffet", query = "SELECT e FROM Effetindesirable e WHERE e.dateeffet = :dateeffet")})
public class Effetindesirable implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @SequenceGenerator( name = "EffetIndesirable", sequenceName = "seq_EffetIndesirable", allocationSize = 1, initialValue = 1 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "EffetIndesirable" )    
    @Column(name = "IDEI")
    private Integer idei;
    @Size(max = 254)
    @Column(name = "RESULTATEI")
    private String resultatei;
    @Size(max = 254)
    @Column(name = "DESCRIPTIONEI")
    private String descriptionei;
    @Size(max = 254)
    @Column(name = "DATEEFFET")
    private String dateeffet;
    @JoinTable(name = "SUBS_EFFET", joinColumns = {
        @JoinColumn(name = "IDEI", referencedColumnName = "IDEI")}, inverseJoinColumns = {
        @JoinColumn(name = "IDSUBS", referencedColumnName = "IDSUBS")})
    @ManyToMany(cascade=CascadeType.PERSIST)
    private List<Substanceactive> substanceactiveList;
    @JoinColumn(name = "USE_ID", referencedColumnName = "ID")
    @ManyToOne
    private Useranonyme useId;
    @JoinColumn(name = "MED_ID", referencedColumnName = "ID")
    @ManyToOne
    private Medecin medId;
    @JoinColumn(name = "ID", referencedColumnName = "ID")
    @ManyToOne
    private Laboratoires id;
    @JoinColumn(name = "CRP_ID", referencedColumnName = "ID")
    @ManyToOne
    private Crpv crpId;
    @JoinColumn(name = "CNP_ID", referencedColumnName = "ID")
    @ManyToOne
    private Cnpv cnpId;
    @JoinColumn(name = "ASS_ID", referencedColumnName = "ID")
    @ManyToOne
    private Associationpatient assId;

    public Effetindesirable() {
    }

    public Effetindesirable(Integer idei) {
        this.idei = idei;
    }

    public Integer getIdei() {
        return idei;
    }

    public void setIdei(Integer idei) {
        this.idei = idei;
    }

    public String getResultatei() {
        return resultatei;
    }

    public void setResultatei(String resultatei) {
        this.resultatei = resultatei;
    }

    public String getDescriptionei() {
        return descriptionei;
    }

    public void setDescriptionei(String descriptionei) {
        this.descriptionei = descriptionei;
    }

    public String getDateeffet() {
        return dateeffet;
    }

    public void setDateeffet(String dateeffet) {
        this.dateeffet = dateeffet;
    }

    @XmlTransient
    public List<Substanceactive> getSubstanceactiveList() {
        return substanceactiveList;
    }

    public void setSubstanceactiveList(List<Substanceactive> substanceactiveList) {
        this.substanceactiveList = substanceactiveList;
    }

    public Useranonyme getUseId() {
        return useId;
    }

    public void setUseId(Useranonyme useId) {
        this.useId = useId;
    }

    public Medecin getMedId() {
        return medId;
    }

    public void setMedId(Medecin medId) {
        this.medId = medId;
    }

    public Laboratoires getId() {
        return id;
    }

    public void setId(Laboratoires id) {
        this.id = id;
    }

    public Crpv getCrpId() {
        return crpId;
    }

    public void setCrpId(Crpv crpId) {
        this.crpId = crpId;
    }

    public Cnpv getCnpId() {
        return cnpId;
    }

    public void setCnpId(Cnpv cnpId) {
        this.cnpId = cnpId;
    }

    public Associationpatient getAssId() {
        return assId;
    }

    public void setAssId(Associationpatient assId) {
        this.assId = assId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idei != null ? idei.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Effetindesirable)) {
            return false;
        }
        Effetindesirable other = (Effetindesirable) object;
        if ((this.idei == null && other.idei != null) || (this.idei != null && !this.idei.equals(other.idei))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Effetindesirable[ idei=" + idei + " ]";
    }
    
}
