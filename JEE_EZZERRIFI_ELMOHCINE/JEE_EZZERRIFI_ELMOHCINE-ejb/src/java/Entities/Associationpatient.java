/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Saadoun
 */
@Entity
@Table(name = "ASSOCIATIONPATIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Associationpatient.findAll", query = "SELECT a FROM Associationpatient a"),
    @NamedQuery(name = "Associationpatient.findById", query = "SELECT a FROM Associationpatient a WHERE a.id = :id"),
    @NamedQuery(name = "Associationpatient.findByNom", query = "SELECT a FROM Associationpatient a WHERE a.nom = :nom"),
    @NamedQuery(name = "Associationpatient.findByAdresse", query = "SELECT a FROM Associationpatient a WHERE a.adresse = :adresse"),
    @NamedQuery(name = "Associationpatient.findByDatecreation", query = "SELECT a FROM Associationpatient a WHERE a.datecreation = :datecreation")})
public class Associationpatient implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @SequenceGenerator( name = "AssociationPatient", sequenceName = "seq_AssociationPatient", allocationSize = 1, initialValue = 1 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "AssociationPatient" )    
    @Column(name = "ID")
    private Integer id;
    @Size(max = 254)
    @Column(name = "NOM")
    private String nom;
    @Size(max = 254)
    @Column(name = "ADRESSE")
    private String adresse;
    @Size(max = 254)
    @Column(name = "DATECREATION")
    private String datecreation;
    @OneToMany(mappedBy = "assId")
    private List<Effetindesirable> effetindesirableList;

    public Associationpatient() {
    }

    public Associationpatient(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(String datecreation) {
        this.datecreation = datecreation;
    }

    @XmlTransient
    public List<Effetindesirable> getEffetindesirableList() {
        return effetindesirableList;
    }

    public void setEffetindesirableList(List<Effetindesirable> effetindesirableList) {
        this.effetindesirableList = effetindesirableList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Associationpatient)) {
            return false;
        }
        Associationpatient other = (Associationpatient) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Associationpatient[ id=" + id + " ]";
    }
    
}
