/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Saadoun
 */
@Entity
@Table(name = "SUBSTANCEACTIVE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Substanceactive.findAll", query = "SELECT s FROM Substanceactive s"),
    @NamedQuery(name = "Substanceactive.findByIdsubs", query = "SELECT s FROM Substanceactive s WHERE s.idsubs = :idsubs"),
    @NamedQuery(name = "Substanceactive.findByLibelle", query = "SELECT s FROM Substanceactive s WHERE s.libelle = :libelle")})
public class Substanceactive implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @SequenceGenerator( name = "SubstanceActive", sequenceName = "seq_SubstanceActive", allocationSize = 1, initialValue = 1 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "SubstanceActive" )
    @Column(name = "IDSUBS")
    private Integer idsubs;
    @Size(max = 254)
    @Column(name = "LIBELLE")
    private String libelle;
    @ManyToMany(mappedBy = "substanceactiveList",cascade=CascadeType.PERSIST)
    private List<Medicament> medicamentList;
    @ManyToMany(mappedBy = "substanceactiveList")
    private List<Effetindesirable> effetindesirableList;

    public Substanceactive() {
    }

    public Substanceactive(Integer idsubs) {
        this.idsubs = idsubs;
    }

    public Integer getIdsubs() {
        return idsubs;
    }

    public void setIdsubs(Integer idsubs) {
        this.idsubs = idsubs;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @XmlTransient
    public List<Medicament> getMedicamentList() {
        return medicamentList;
    }

    public void setMedicamentList(List<Medicament> medicamentList) {
        this.medicamentList = medicamentList;
    }

    @XmlTransient
    public List<Effetindesirable> getEffetindesirableList() {
        return effetindesirableList;
    }

    public void setEffetindesirableList(List<Effetindesirable> effetindesirableList) {
        this.effetindesirableList = effetindesirableList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idsubs != null ? idsubs.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Substanceactive)) {
            return false;
        }
        Substanceactive other = (Substanceactive) object;
        if ((this.idsubs == null && other.idsubs != null) || (this.idsubs != null && !this.idsubs.equals(other.idsubs))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Substanceactive[ idsubs=" + idsubs + 
                                        " Libelle= " +libelle +
                                        " ]";
    }
    
}
