package Entities;

import Entities.Associationpatient;
import Entities.Cnpv;
import Entities.Crpv;
import Entities.Laboratoires;
import Entities.Medecin;
import Entities.Substanceactive;
import Entities.Useranonyme;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-01-12T17:07:03")
@StaticMetamodel(Effetindesirable.class)
public class Effetindesirable_ { 

    public static volatile SingularAttribute<Effetindesirable, Laboratoires> id;
    public static volatile SingularAttribute<Effetindesirable, String> descriptionei;
    public static volatile SingularAttribute<Effetindesirable, Useranonyme> useId;
    public static volatile SingularAttribute<Effetindesirable, Medecin> medId;
    public static volatile SingularAttribute<Effetindesirable, Crpv> crpId;
    public static volatile SingularAttribute<Effetindesirable, Integer> idei;
    public static volatile SingularAttribute<Effetindesirable, String> dateeffet;
    public static volatile ListAttribute<Effetindesirable, Substanceactive> substanceactiveList;
    public static volatile SingularAttribute<Effetindesirable, Associationpatient> assId;
    public static volatile SingularAttribute<Effetindesirable, Cnpv> cnpId;
    public static volatile SingularAttribute<Effetindesirable, String> resultatei;

}