package Entities;

import Entities.Effetindesirable;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-01-12T17:07:03")
@StaticMetamodel(Medecin.class)
public class Medecin_ { 

    public static volatile SingularAttribute<Medecin, Integer> id;
    public static volatile SingularAttribute<Medecin, String> prenom;
    public static volatile SingularAttribute<Medecin, String> specialite;
    public static volatile ListAttribute<Medecin, Effetindesirable> effetindesirableList;
    public static volatile SingularAttribute<Medecin, String> adresse;
    public static volatile SingularAttribute<Medecin, String> nom;

}