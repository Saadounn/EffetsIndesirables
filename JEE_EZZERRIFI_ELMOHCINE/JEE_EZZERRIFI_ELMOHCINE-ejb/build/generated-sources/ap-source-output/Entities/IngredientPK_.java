package Entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-01-12T17:07:03")
@StaticMetamodel(IngredientPK.class)
public class IngredientPK_ { 

    public static volatile SingularAttribute<IngredientPK, Integer> id;
    public static volatile SingularAttribute<IngredientPK, Integer> idingredient;
    public static volatile SingularAttribute<IngredientPK, Integer> medId;

}