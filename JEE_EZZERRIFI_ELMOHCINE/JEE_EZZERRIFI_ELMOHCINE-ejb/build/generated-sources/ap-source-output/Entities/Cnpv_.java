package Entities;

import Entities.Effetindesirable;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-01-12T17:07:03")
@StaticMetamodel(Cnpv.class)
public class Cnpv_ { 

    public static volatile SingularAttribute<Cnpv, Integer> id;
    public static volatile ListAttribute<Cnpv, Effetindesirable> effetindesirableList;
    public static volatile SingularAttribute<Cnpv, String> adresse;
    public static volatile SingularAttribute<Cnpv, String> nom;

}