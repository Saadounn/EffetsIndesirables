package Entities;

import Entities.Effetindesirable;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-01-12T17:07:03")
@StaticMetamodel(Useranonyme.class)
public class Useranonyme_ { 

    public static volatile SingularAttribute<Useranonyme, Integer> id;
    public static volatile SingularAttribute<Useranonyme, String> prenom;
    public static volatile ListAttribute<Useranonyme, Effetindesirable> effetindesirableList;
    public static volatile SingularAttribute<Useranonyme, String> adresse;
    public static volatile SingularAttribute<Useranonyme, String> nom;

}