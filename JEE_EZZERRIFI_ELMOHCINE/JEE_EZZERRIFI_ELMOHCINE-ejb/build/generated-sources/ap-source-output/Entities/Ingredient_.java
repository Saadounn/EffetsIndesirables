package Entities;

import Entities.IngredientPK;
import Entities.Medicament;
import Entities.Prodcosmetique;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-01-12T17:07:03")
@StaticMetamodel(Ingredient.class)
public class Ingredient_ { 

    public static volatile SingularAttribute<Ingredient, IngredientPK> ingredientPK;
    public static volatile SingularAttribute<Ingredient, Prodcosmetique> prodcosmetique;
    public static volatile SingularAttribute<Ingredient, Medicament> medicament;
    public static volatile SingularAttribute<Ingredient, String> principesactifs;
    public static volatile SingularAttribute<Ingredient, String> conservateurs;
    public static volatile SingularAttribute<Ingredient, String> excipient;

}