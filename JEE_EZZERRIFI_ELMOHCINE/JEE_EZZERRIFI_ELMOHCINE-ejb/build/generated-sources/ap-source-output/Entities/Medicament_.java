package Entities;

import Entities.Ingredient;
import Entities.Substanceactive;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-01-12T17:07:03")
@StaticMetamodel(Medicament.class)
public class Medicament_ { 

    public static volatile SingularAttribute<Medicament, Integer> id;
    public static volatile ListAttribute<Medicament, Ingredient> ingredientList;
    public static volatile SingularAttribute<Medicament, String> nomfabricant;
    public static volatile SingularAttribute<Medicament, String> dureetraitement;
    public static volatile SingularAttribute<Medicament, String> libelle;
    public static volatile SingularAttribute<Medicament, String> indication;
    public static volatile ListAttribute<Medicament, Substanceactive> substanceactiveList;
    public static volatile SingularAttribute<Medicament, String> dateexpiration;

}