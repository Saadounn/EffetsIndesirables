package Entities;

import Entities.Ingredient;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-01-12T17:07:03")
@StaticMetamodel(Prodcosmetique.class)
public class Prodcosmetique_ { 

    public static volatile SingularAttribute<Prodcosmetique, Integer> id;
    public static volatile ListAttribute<Prodcosmetique, Ingredient> ingredientList;
    public static volatile SingularAttribute<Prodcosmetique, String> nomfabricant;
    public static volatile SingularAttribute<Prodcosmetique, String> dureetraitement;
    public static volatile SingularAttribute<Prodcosmetique, String> indication;
    public static volatile SingularAttribute<Prodcosmetique, String> dateexpiration;

}