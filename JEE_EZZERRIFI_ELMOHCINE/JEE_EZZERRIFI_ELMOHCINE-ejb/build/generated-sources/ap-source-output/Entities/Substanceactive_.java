package Entities;

import Entities.Effetindesirable;
import Entities.Medicament;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-01-12T17:07:03")
@StaticMetamodel(Substanceactive.class)
public class Substanceactive_ { 

    public static volatile ListAttribute<Substanceactive, Effetindesirable> effetindesirableList;
    public static volatile SingularAttribute<Substanceactive, String> libelle;
    public static volatile SingularAttribute<Substanceactive, Integer> idsubs;
    public static volatile ListAttribute<Substanceactive, Medicament> medicamentList;

}