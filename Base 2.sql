


/*==============================================================*/
/* Table : AssociationPatient                                   */
/*==============================================================*/
create sequence seq_AssociationPatient start with 1;


/*==============================================================*/
/* Table : Cnpv                                                 */
/*==============================================================*/
create sequence seq_Cnpv start with 1;



/*==============================================================*/
/* Table : Crpv                                                 */
/*==============================================================*/
create sequence seq_Crpv start with 1;

/*==============================================================*/
/* Table : EffetIndesirable                                     */
/*==============================================================*/
create sequence seq_EffetIndesirable start with 1;

/*==============================================================*/
/* Table : Ingredient                                           */
/*==============================================================*/
create sequence seq_Ingredient start with 1;


/*==============================================================*/
/* Table : Laboratoires                                         */
/*==============================================================*/
create sequence seq_Laboratoires start with 1;



/*==============================================================*/
/* Table : Medecin                                              */
/*==============================================================*/
create sequence seq_Medecin start with 1;



/*==============================================================*/
/* Table : Medicament                                           */
/*==============================================================*/
create sequence seq_Medicament start with 1;



/*==============================================================*/
/* Table : ProdCosmetique                                       */
/*==============================================================*/
create sequence seq_ProdCosmetique start with 1;



/*==============================================================*/
/* Table : SubstanceActive                                      */
/*==============================================================*/
create sequence seq_SubstanceActive start with 1;


/*==============================================================*/
/* Table : UserAnonyme                                          */
/*==============================================================*/
create sequence seq_UserAnonyme start with 1;


/*==============================================================*/
/* Table : subs_med                                        */
/*==============================================================*/
create sequence seq_subs_med start with 1;



/*==============================================================*/
/* Table : subs_effet                                         */
/*==============================================================*/
create sequence seq_subs_effet start with 1;



/*==============================================================*/
/* Table : AssociationPatient                                   */
/*==============================================================*/
create table AssociationPatient 
(
   id                   integer                        not null,
   nom                  varchar(254)                   null,
   adresse              varchar(254)                   null,
   dateCreation         varchar(254)                   null,
   constraint PKONPATIENT primary key (id)
);


/*==============================================================*/
/* Table : Cnpv                                                 */
/*==============================================================*/
create table Cnpv 
(
   id                   integer                        not null,
   nom                  varchar(254)                   null,
   adresse              varchar(254)                   null,
   constraint PK_CNPV primary key (id)
);



/*==============================================================*/
/* Table : Crpv                                                 */
/*==============================================================*/
create table Crpv 
(
   id                   integer                        not null,
   nom                  varchar(254)                   null,
   adresse              varchar(254)                   null,
   constraint PK_CRPV primary key (id)
);

/*==============================================================*/
/* Table : EffetIndesirable                                     */
/*==============================================================*/
create table EffetIndesirable 
(
   idEi                 integer                        not null,
   id                   integer                        not null,
   Ass_id               integer                        not null,
   Use_id               integer                        not null,
   Crp_id               integer                        not null,
   Med_id               integer                        not null,
   Cnp_id               integer                        not null,
   resultatEi           varchar(254)                   null,
   descriptionEi        varchar(254)                   null,
   dateEffet            date                           null,
   constraint PK_EFFETINDESIRABLE primary key (idEi));

/*==============================================================*/
/* Table : Ingredient                                           */
/*==============================================================*/
create table Ingredient 
(
   id                   integer                        not null,
   Med_id               integer                        not null,
   idIngredient         integer                        not null,
   principesActifs      varchar(254)                   null,
   conservateurs        varchar(254)                   null,
   excipient            varchar(254)                   null,
   constraint PK_INGREDIENT primary key (id, Med_id, idIngredient)
);


/*==============================================================*/
/* Table : Laboratoires                                         */
/*==============================================================*/
create table Laboratoires 
(
   id                   integer                        not null,
   nom                  varchar(254)                   null,
   adresse              varchar(254)                   null,
   type                 varchar(254)                   null,
   constraint PK_LABORATOIRES primary key (id)
);



/*==============================================================*/
/* Table : Medecin                                              */
/*==============================================================*/
create table Medecin 
(
   id                   integer                        not null,
   prenom               varchar(254)                   null,
   nom                  varchar(254)                   null,
   adresse              varchar(254)                   null,
   specialite           varchar(254)                   null,
   constraint PK_MEDECIN primary key (id)
);



/*==============================================================*/
/* Table : Medicament                                           */
/*==============================================================*/
create table Medicament 
(
   id                   integer                        not null,
   nom                  varchar(254)                   not null,
   indication           varchar(254)                   null,
   nomFabricant         varchar(254)                   null,
   dureeTraitement      varchar(254)                   null,
   dateExpiration       date                           nlul,
   constraint PK_MEDICAMENT primary key (id)
);



/*==============================================================*/
/* Table : ProdCosmetique                                       */
/*==============================================================*/
create table ProdCosmetique 
(
   id                   integer                        not null,
   indication           varchar(254)                   null,
   nomFabricant         varchar(254)                   null,
   dureeTraitement      date                      null,
   dateExpiration       date                      null,
   constraint PK_PRODCOSMETIQUE primary key (id)
);



/*==============================================================*/
/* Table : SubstanceActive                                      */
/*==============================================================*/
create table SubstanceActive 
(
   idSubs               integer                        not null,
   libelle              varchar(254)                   null,
   constraint PK_SUBSTANCEACTIVE primary key (idSubs)
);


/*==============================================================*/
/* Table : UserAnonyme                                          */
/*==============================================================*/
create table UserAnonyme 
(
   id                   integer                        not null,
   prenom               varchar(254)                    null,
   nom                  varchar(254)                   null,
   adresse              varchar(254)                   null,
   constraint PK_USERANONYME primary key (id)
);


/*==============================================================*/
/* Table : subs_med                                        */
/*==============================================================*/
create table subs_med 
(
   id                   integer                        not null,
   idSubs               integer                        not null,
   constraint PKON13 primary key  (id, idSubs)
);



/*==============================================================*/
/* Table : subs_effet                                         */
/*==============================================================*/
create table subs_effet 
(
   idEi                 integer                        not null,
   idSubs               integer                        not null,
   constraint PKON4 primary key  (idEi, idSubs)
);


alter table EffetIndesirable
   add constraint FK_EFFETIND_ASSOCIAT foreign key (Ass_id)
      references AssociationPatient (id);

alter table EffetIndesirable
   add constraint FK_EFFETIND_CRPV foreign key (Crp_id)
      references Crpv (id);

alter table EffetIndesirable
   add constraint FK_EFFETIND_CNPV foreign key (Cnp_id)
      references Cnpv (id);

alter table EffetIndesirable
   add constraint FK_EFFETIND_MEDECIN foreign key (Med_id)
      references Medecin (id);

alter table EffetIndesirable
   add constraint FK_EFFETIND_LABORATO foreign key (id)
      references Laboratoires (id);

alter table EffetIndesirable
   add constraint FK_EFFETIND_USERANON foreign key (Use_id)
      references UserAnonyme (id);

alter table Ingredient
   add constraint FK_INGREDIE_PRODCOSM foreign key (id)
      references ProdCosmetique (id);

alter table Ingredient
   add constraint FK_INGREDIE_MEDICAME foreign key (Med_id)
      references Medicament (id);

alter table subs_med
   add constraint FK_MEDICAME foreign key (id)
      references Medicament (id);

alter table subs_med
   add constraint FK_SUBSTANC foreign key (idSubs)
      references SubstanceActive (idSubs);

alter table subs_effet
   add constraint FK_EFFETIND foreign key (idEi)
      references EffetIndesirable (idEi);

alter table subs_effet
   add constraint FK_effet_SUBSTANC foreign key (idSubs)
      references SubstanceActive (idSubs);

