<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>NotifEI | Ajout des anonymes pharmaceutiques</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <!-- DATA TABLES -->
        <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="../index.html" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                NotifEI
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">


             
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>Jane Doe <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="img/avatar2.png" class="img-circle" alt="User Image" />
                                    <p>
                                        Jane Doe - Web Developer
                                        <small>Member since Nov. 2012</small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src=" img/avatar2.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, Jane</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="post" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Rechercher..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    
                    <%@include file="__Menu.jsp" %>


                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Déclaration d'un effet indésirable 
                        <small>(Utilisateur non connecté)</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Utilisateurs</a></li>
                        <li><a href="#">Anonymes</a></li>
                        <li class="active">Nouveau</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                                
                            <form action="GestionEffetsIndesirableServelet" method="post">
                                <input type="hidden" name="Libelle" value="<%=request.getAttribute("Libelle") %>" >
                                <input type="hidden" name="Description" value="<%=request.getAttribute("Description")%>" >
                                <input type="hidden" name="Date" value="<%=request.getAttribute("Date")%>" >

                                <input type = "hidden" name ="Libelle1" value ="<%=request.getAttribute("Libelle1") %>" >
                                <input type = "hidden" name ="Indication1" value ="<%=request.getAttribute("Indication1") %>" >
                                <input type = "hidden" name ="Substance1" value ="<%=request.getAttribute("Substance1") %>" >
                                <input type = "hidden" name ="Fabricant1" value ="<%=request.getAttribute("Fabricant1") %>" >
                                <input type = "hidden" name ="Duree1" value ="<%=request.getAttribute("Duree1") %>" >
                                <input type = "hidden" name ="Date1" value ="<%=request.getAttribute("Date1") %>" >

                                <input type = "hidden" name ="Libelle2" value ="<%=request.getAttribute("Libelle2") %>" >
                                <input type = "hidden" name ="Indication2" value ="<%=request.getAttribute("Indication2") %>" >
                                <input type = "hidden" name ="Substance2" value ="<%=request.getAttribute("Substance2") %>" >
                                <input type = "hidden" name ="Fabricant2" value ="<%=request.getAttribute("Fabricant2") %>" >
                                <input type = "hidden" name ="Duree2" value ="<%=request.getAttribute("Duree2") %>" >
                                <input type = "hidden" name ="Date2" value ="<%=request.getAttribute("Date2") %>" >

                                <input type = "hidden" name ="Libelle3" value ="<%=request.getAttribute("Libelle3") %>" >
                                <input type = "hidden" name ="Indication3" value ="<%=request.getAttribute("Indication3") %>" >
                                <input type = "hidden" name ="Substance3" value ="<%=request.getAttribute("Substance3") %>" >
                                <input type = "hidden" name ="Fabricant3" value ="<%=request.getAttribute("Fabricant3") %>" >
                                <input type = "hidden" name ="Duree3" value ="<%=request.getAttribute("Duree3") %>" >
                                <input type = "hidden" name ="Date3" value ="<%=request.getAttribute("Date3") %>" >

                                <input type = "hidden" name ="Libelle4" value ="<%=request.getAttribute("Libelle4") %>" >
                                <input type = "hidden" name ="Indication4" value ="<%=request.getAttribute("Indication4") %>" >
                                <input type = "hidden" name ="Substance4" value ="<%=request.getAttribute("Substance4") %>" >
                                <input type = "hidden" name ="Fabricant4" value ="<%=request.getAttribute("Fabricant4") %>" >
                                <input type = "hidden" name ="Duree4" value ="<%=request.getAttribute("Duree4") %>" >
                                <input type = "hidden" name ="Date4" value ="<%=request.getAttribute("Date4") %>" >

                                <input type = "hidden" name ="Libelle5" value ="<%=request.getAttribute("Libelle5") %>" >
                                <input type = "hidden" name ="Indication5" value ="<%=request.getAttribute("Indication5") %>" >
                                <input type = "hidden" name ="Substance5" value ="<%=request.getAttribute("Substance5") %>" >
                                <input type = "hidden" name ="Fabricant5" value ="<%=request.getAttribute("Fabricant5") %>" >
                                <input type = "hidden" name ="Duree5" value ="<%=request.getAttribute("Duree5") %>" >
                                <input type = "hidden" name ="Date5" value ="<%=request.getAttribute("Date5") %>">
                                
                                
                                
                                <!-- general form elements -->
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title">Informations du déclarant</h3>
                                    </div><!-- /.box-header -->
                                    <!-- form start -->
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label for="Nom">Nom : </label>
                                                <input type="text" name ="Nom" class="form-control" id="Nom" placeholder="Nom ...">
                                            </div>
                                            <div class="form-group">
                                                <label for="Prenom">Prénom : </label>
                                                <input type="text" name ="Prenom" class="form-control" id="Prenom" placeholder="Prénom ...">
                                            </div>
                                            <div class="form-group">
                                                <label for="Adresse">Adresse : </label>
                                                <input type="text" name ="Adresse" class="form-control" id="Adresse" placeholder="N° Rue, Ville...">
                                            </div>

                                        </div><!-- /.box-body -->

                                        <div class="box-footer">
                                            
                                        </div>
                                </div><!-- /.box -->
                                <button type="submit" class="btn btn-primary pull-right" name="Action" value="Terminer">Terminer</button>
                                            
                                <a type="Button" href="GestionAnonymesServelet" class="btn btn-danger">Annuler</a>
                            </form>

                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
            
            
        </script>

    </body>
</html>