/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlles;

import Entities.Laboratoires;
import Models.LaboratoiresFacadeLocal;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Saadoun
 */
@WebServlet(name = "GestionLaboratoireServelet", urlPatterns = {"/GestionLaboratoireServelet"})

public class GestionLaboratoireServelet extends HttpServlet {

    @EJB
    public LaboratoiresFacadeLocal Labos; 
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
         String action = request.getParameter("Action");
        
        if( action!=null && action.equals(("Delete"))){
            
            Laboratoires theLab = Labos.find(parseInt(request.getParameter("IdLabo")));
            
            Labos.remove(theLab);
            
        request.setAttribute("ListLaboratoires", Labos.findAll());
        
        request.getRequestDispatcher("pages/List_Laboratoires.jsp").forward(request, response);
            
        }else{
            if(action!=null && action.equals("Edit")){

                if( Labos.find(parseInt(request.getParameter("IdLabo"))) != null){
                Laboratoires theLab =  Labos.find(parseInt(request.getParameter("IdLabo")));

                request.setAttribute("labo", theLab);
                request.getRequestDispatcher("pages/Modif_Laboratoire.jsp").forward(request, response);
                }
                request.getRequestDispatcher("pages/Error.jsp").forward(request, response);
            }else{
                if(action!=null && action.equals("ValidEdit")){
                    
                    Laboratoires theLab = new Laboratoires();
                    
                    theLab.setId(Integer.parseInt(request.getParameter("IdLab")));
                    theLab.setNom(request.getParameter("Nom"));
                    theLab.setAdresse(request.getParameter("Adresse"));
                    theLab.setType(request.getParameter("Type"));
                    
                    Labos.edit(theLab);
                    
                    request.setAttribute("ListLaboratoires", Labos.findAll());
                    request.getRequestDispatcher("pages/List_Laboratoires.jsp").forward(request, response);
                }else{
                    if(action!=null && action.equals("Detail")){
                        if( Labos.find(parseInt(request.getParameter("IdLabo"))) != null){
                            Laboratoires theLab =  Labos.find(parseInt(request.getParameter("IdLabo")));

                            
                            request.setAttribute("labo", theLab);
                            request.getRequestDispatcher("pages/Detail_Laboratoires.jsp").forward(request, response);
                        }
                        request.getRequestDispatcher("pages/Error.jsp").forward(request, response);
                        
                    }
                }
            }
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
