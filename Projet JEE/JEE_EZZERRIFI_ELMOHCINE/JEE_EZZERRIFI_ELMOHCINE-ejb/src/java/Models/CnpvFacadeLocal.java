/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Entities.Cnpv;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Saadoun
 */
@Local
public interface CnpvFacadeLocal {

    void create(Cnpv cnpv);

    void edit(Cnpv cnpv);

    void remove(Cnpv cnpv);

    Cnpv find(Object id);

    List<Cnpv> findAll();

    List<Cnpv> findRange(int[] range);

    int count();
    
}
