/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Entities.Associationpatient;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Saadoun
 */
@Local
public interface AssociationpatientFacadeLocal {

    void create(Associationpatient associationpatient);

    void edit(Associationpatient associationpatient);

    void remove(Associationpatient associationpatient);

    Associationpatient find(Object id);

    List<Associationpatient> findAll();

    List<Associationpatient> findRange(int[] range);

    int count();
    
}
