/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Entities.Laboratoires;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Saadoun
 */
@Local
public interface LaboratoiresFacadeLocal {

    void create(Laboratoires laboratoires);

    void edit(Laboratoires laboratoires);

    void remove(Laboratoires laboratoires);

    Laboratoires find(Object id);

    List<Laboratoires> findAll();

    List<Laboratoires> findRange(int[] range);

    int count();
    
}
