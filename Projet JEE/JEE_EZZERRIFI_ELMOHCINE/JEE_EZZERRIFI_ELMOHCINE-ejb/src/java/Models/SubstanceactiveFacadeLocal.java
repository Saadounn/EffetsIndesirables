/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Entities.Substanceactive;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Saadoun
 */
@Local
public interface SubstanceactiveFacadeLocal {

    void create(Substanceactive substanceactive);

    void edit(Substanceactive substanceactive);

    void remove(Substanceactive substanceactive);

    Substanceactive find(Object id);

    List<Substanceactive> findAll();

    List<Substanceactive> findRange(int[] range);

    int count();
    
}
