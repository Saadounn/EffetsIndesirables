/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Entities.Crpv;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Saadoun
 */
@Stateless
public class CrpvFacade extends AbstractFacade<Crpv> implements CrpvFacadeLocal {
    @PersistenceContext(unitName = "JEE_EZZERRIFI_ELMOHCINE-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CrpvFacade() {
        super(Crpv.class);
    }
    
}
