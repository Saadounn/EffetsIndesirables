/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Entities.Useranonyme;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Saadoun
 */
@Local
public interface UseranonymeFacadeLocal {

    void create(Useranonyme useranonyme);

    void edit(Useranonyme useranonyme);

    void remove(Useranonyme useranonyme);

    Useranonyme find(Object id);

    List<Useranonyme> findAll();

    List<Useranonyme> findRange(int[] range);

    int count();
    
}
