/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Saadoun
 */
@Entity
@Table(name = "PRODCOSMETIQUE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Prodcosmetique.findAll", query = "SELECT p FROM Prodcosmetique p"),
    @NamedQuery(name = "Prodcosmetique.findById", query = "SELECT p FROM Prodcosmetique p WHERE p.id = :id"),
    @NamedQuery(name = "Prodcosmetique.findByIndication", query = "SELECT p FROM Prodcosmetique p WHERE p.indication = :indication"),
    @NamedQuery(name = "Prodcosmetique.findByNomfabricant", query = "SELECT p FROM Prodcosmetique p WHERE p.nomfabricant = :nomfabricant"),
    @NamedQuery(name = "Prodcosmetique.findByDureetraitement", query = "SELECT p FROM Prodcosmetique p WHERE p.dureetraitement = :dureetraitement"),
    @NamedQuery(name = "Prodcosmetique.findByDateexpiration", query = "SELECT p FROM Prodcosmetique p WHERE p.dateexpiration = :dateexpiration")})
public class Prodcosmetique implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation

    @SequenceGenerator( name = "CosmeSeq", sequenceName = "seq_ProdCosmetique", allocationSize = 1, initialValue = 1 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "CosmeSeq" )
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 254)
    @Column(name = "INDICATION")
    private String indication;
    @Size(max = 254)
    @Column(name = "NOMFABRICANT")
    private String nomfabricant;
    @Size(max = 254)
    @Column(name = "DUREETRAITEMENT")
    private String dureetraitement;
    @Size(max = 254)    
    @Column(name = "DATEEXPIRATION")
    private String dateexpiration;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "prodcosmetique")
    private List<Ingredient> ingredientList;

    public Prodcosmetique() {
    }

    public Prodcosmetique(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIndication() {
        return indication;
    }

    public void setIndication(String indication) {
        this.indication = indication;
    }

    public String getNomfabricant() {
        return nomfabricant;
    }

    public void setNomfabricant(String nomfabricant) {
        this.nomfabricant = nomfabricant;
    }

    public String getDureetraitement() {
        return dureetraitement;
    }

    public void setDureetraitement(String dureetraitement) {
        this.dureetraitement = dureetraitement;
    }

    public String getDateexpiration() {
        return dateexpiration;
    }

    public void setDateexpiration(String dateexpiration) {
        this.dateexpiration = dateexpiration;
    }

    @XmlTransient
    public List<Ingredient> getIngredientList() {
        return ingredientList;
    }

    public void setIngredientList(List<Ingredient> ingredientList) {
        this.ingredientList = ingredientList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prodcosmetique)) {
            return false;
        }
        Prodcosmetique other = (Prodcosmetique) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Prodcosmetique[ id=" + id + " ]";
    }
    
}
