/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Saadoun
 */
@Entity
@Table(name = "MEDICAMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Medicament.findAll", query = "SELECT m FROM Medicament m"),
    @NamedQuery(name = "Medicament.findById", query = "SELECT m FROM Medicament m WHERE m.id = :id"),
    @NamedQuery(name = "Medicament.findByIndication", query = "SELECT m FROM Medicament m WHERE m.indication = :indication"),
    @NamedQuery(name = "Medicament.findByNomfabricant", query = "SELECT m FROM Medicament m WHERE m.nomfabricant = :nomfabricant"),
    @NamedQuery(name = "Medicament.findByDureetraitement", query = "SELECT m FROM Medicament m WHERE m.dureetraitement = :dureetraitement"),
    @NamedQuery(name = "Medicament.findByDateexpiration", query = "SELECT m FROM Medicament m WHERE m.dateexpiration = :dateexpiration")})
public class Medicament implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    
    @SequenceGenerator( name = "MedocSeq", sequenceName = "seq_Medicament", allocationSize = 1, initialValue = 1 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "MedocSeq" )
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 254)
    @Column(name = "NOM")
    private String nom;
    @Size(max = 254)
    @Column(name = "INDICATION")
    private String indication;
    @Size(max = 254)
    @Column(name = "NOMFABRICANT")
    private String nomfabricant;
    @Size(max = 254)
    @Column(name = "DUREETRAITEMENT")
    private String dureetraitement;
    @Size(max = 254)
    @Column(name = "DATEEXPIRATION")
    private String dateexpiration;
    @JoinTable(name = "SUBS_MED", joinColumns = {
        @JoinColumn(name = "ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "IDSUBS", referencedColumnName = "IDSUBS")})
    @ManyToMany(mappedBy = "substanceactiveList")
    private List<Substanceactive> substanceactiveList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "medicament")
    private List<Ingredient> ingredientList;

    public Medicament() {
    }

    public Medicament(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom(){
        return this.nom;
    }
    
    public void setNom(String nom){
        this.nom = nom;
    }
    
    public String getIndication() {
        return indication;
    }

    public void setIndication(String indication) {
        this.indication = indication;
    }

    public String getNomfabricant() {
        return nomfabricant;
    }

    public void setNomfabricant(String nomfabricant) {
        this.nomfabricant = nomfabricant;
    }

    public String getDureetraitement() {
        return dureetraitement;
    }

    public void setDureetraitement(String dureetraitement) {
        this.dureetraitement = dureetraitement;
    }

    public String getDateexpiration() {
        return dateexpiration;
    }

    public void setDateexpiration(String dateexpiration) {
        this.dateexpiration = dateexpiration;
    }

    @XmlTransient
    public List<Substanceactive> getSubstanceactiveList() {
        return substanceactiveList;
    }

    public void setSubstanceactiveList(List<Substanceactive> substanceactiveList) {
        this.substanceactiveList = substanceactiveList;
    }

    @XmlTransient
    public List<Ingredient> getIngredientList() {
        return ingredientList;
    }

    public void setIngredientList(List<Ingredient> ingredientList) {
        this.ingredientList = ingredientList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Medicament)) {
            return false;
        }
        Medicament other = (Medicament) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Medicament[ id=" + id + " ]";
    }
    
}
