/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Saadoun
 */
@Embeddable
public class IngredientPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigInteger id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MED_ID")
    private BigInteger medId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDINGREDIENT")
    private BigInteger idingredient;

    public IngredientPK() {
    }

    public IngredientPK(BigInteger id, BigInteger medId, BigInteger idingredient) {
        this.id = id;
        this.medId = medId;
        this.idingredient = idingredient;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getMedId() {
        return medId;
    }

    public void setMedId(BigInteger medId) {
        this.medId = medId;
    }

    public BigInteger getIdingredient() {
        return idingredient;
    }

    public void setIdingredient(BigInteger idingredient) {
        this.idingredient = idingredient;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        hash += (medId != null ? medId.hashCode() : 0);
        hash += (idingredient != null ? idingredient.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IngredientPK)) {
            return false;
        }
        IngredientPK other = (IngredientPK) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        if ((this.medId == null && other.medId != null) || (this.medId != null && !this.medId.equals(other.medId))) {
            return false;
        }
        if ((this.idingredient == null && other.idingredient != null) || (this.idingredient != null && !this.idingredient.equals(other.idingredient))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.IngredientPK[ id=" + id + ", medId=" + medId + ", idingredient=" + idingredient + " ]";
    }
    
}
