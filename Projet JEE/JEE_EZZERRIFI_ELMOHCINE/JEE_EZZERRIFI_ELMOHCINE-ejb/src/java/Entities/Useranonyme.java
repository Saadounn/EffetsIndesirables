/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Saadoun
 */
@Entity
@Table(name = "USERANONYME")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Useranonyme.findAll", query = "SELECT u FROM Useranonyme u"),
    @NamedQuery(name = "Useranonyme.findById", query = "SELECT u FROM Useranonyme u WHERE u.id = :id"),
    @NamedQuery(name = "Useranonyme.findByPrenom", query = "SELECT u FROM Useranonyme u WHERE u.prenom = :prenom"),
    @NamedQuery(name = "Useranonyme.findByNom", query = "SELECT u FROM Useranonyme u WHERE u.nom = :nom"),
    @NamedQuery(name = "Useranonyme.findByAdresse", query = "SELECT u FROM Useranonyme u WHERE u.adresse = :adresse")})
public class Useranonyme implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    
    @SequenceGenerator( name = "UserSeq", sequenceName = "seq_UserAnonyme", allocationSize = 1, initialValue = 1 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "UserSeq" )
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 254)
    @Column(name = "PRENOM")
    private String prenom;
    @Size(max = 254)
    @Column(name = "NOM")
    private String nom;
    @Size(max = 254)
    @Column(name = "ADRESSE")
    private String adresse;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "useId")
    private List<Effetindesirable> effetindesirableList;

    public Useranonyme() {
    }

    public Useranonyme(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @XmlTransient
    public List<Effetindesirable> getEffetindesirableList() {
        return effetindesirableList;
    }

    public void setEffetindesirableList(List<Effetindesirable> effetindesirableList) {
        this.effetindesirableList = effetindesirableList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Useranonyme)) {
            return false;
        }
        Useranonyme other = (Useranonyme) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Useranonyme[ id=" + id + " ]";
    }
    
}
