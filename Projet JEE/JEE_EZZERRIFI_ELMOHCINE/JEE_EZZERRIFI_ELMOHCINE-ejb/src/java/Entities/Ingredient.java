/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Saadoun
 */
@Entity
@Table(name = "INGREDIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ingredient.findAll", query = "SELECT i FROM Ingredient i"),
    @NamedQuery(name = "Ingredient.findById", query = "SELECT i FROM Ingredient i WHERE i.ingredientPK.id = :id"),
    @NamedQuery(name = "Ingredient.findByMedId", query = "SELECT i FROM Ingredient i WHERE i.ingredientPK.medId = :medId"),
    @NamedQuery(name = "Ingredient.findByIdingredient", query = "SELECT i FROM Ingredient i WHERE i.ingredientPK.idingredient = :idingredient"),
    @NamedQuery(name = "Ingredient.findByPrincipesactifs", query = "SELECT i FROM Ingredient i WHERE i.principesactifs = :principesactifs"),
    @NamedQuery(name = "Ingredient.findByConservateurs", query = "SELECT i FROM Ingredient i WHERE i.conservateurs = :conservateurs"),
    @NamedQuery(name = "Ingredient.findByExcipient", query = "SELECT i FROM Ingredient i WHERE i.excipient = :excipient")})
public class Ingredient implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected IngredientPK ingredientPK;
    @Size(max = 254)
    @Column(name = "PRINCIPESACTIFS")
    private String principesactifs;
    @Size(max = 254)
    @Column(name = "CONSERVATEURS")
    private String conservateurs;
    @Size(max = 254)
    @Column(name = "EXCIPIENT")
    private String excipient;
    @JoinColumn(name = "ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Prodcosmetique prodcosmetique;
    @JoinColumn(name = "MED_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Medicament medicament;

    public Ingredient() {
    }

    public Ingredient(IngredientPK ingredientPK) {
        this.ingredientPK = ingredientPK;
    }

    public Ingredient(BigInteger id, BigInteger medId, BigInteger idingredient) {
        this.ingredientPK = new IngredientPK(id, medId, idingredient);
    }

    public IngredientPK getIngredientPK() {
        return ingredientPK;
    }

    public void setIngredientPK(IngredientPK ingredientPK) {
        this.ingredientPK = ingredientPK;
    }

    public String getPrincipesactifs() {
        return principesactifs;
    }

    public void setPrincipesactifs(String principesactifs) {
        this.principesactifs = principesactifs;
    }

    public String getConservateurs() {
        return conservateurs;
    }

    public void setConservateurs(String conservateurs) {
        this.conservateurs = conservateurs;
    }

    public String getExcipient() {
        return excipient;
    }

    public void setExcipient(String excipient) {
        this.excipient = excipient;
    }

    public Prodcosmetique getProdcosmetique() {
        return prodcosmetique;
    }

    public void setProdcosmetique(Prodcosmetique prodcosmetique) {
        this.prodcosmetique = prodcosmetique;
    }

    public Medicament getMedicament() {
        return medicament;
    }

    public void setMedicament(Medicament medicament) {
        this.medicament = medicament;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ingredientPK != null ? ingredientPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ingredient)) {
            return false;
        }
        Ingredient other = (Ingredient) object;
        if ((this.ingredientPK == null && other.ingredientPK != null) || (this.ingredientPK != null && !this.ingredientPK.equals(other.ingredientPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Ingredient[ ingredientPK=" + ingredientPK + " ]";
    }
    
}
